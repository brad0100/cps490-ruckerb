
var http = require('http')
var app = require('express')()
var server = http.createServer(app)
const port = process.env.PORT || 8081
server.listen(port);
console.log(`Express HTTP Server is listening at port ${port}`)
var io = require('socket.io');
var socketio = io.listen(server);
console.log("Socket.IO is listening at port: " + port);
app.get('/', (request, response) => {
  console.log("Got an HTTP request")  
  response.sendFile(__dirname+'/index.html')
})
//Server Listening for connections

socketio.on("connection", function (socketclient)
{
console.log("A new Socket.IO client is connected. ID= " + socketclient.id);
socketclient.on("login", (username,password)=>
{

console.log("Debug>Got username="+username +
";password="+password);
if(DataLayer.checklogin(username,password))
{
    socketclient.username = username;
    socketclient.password = password;
    socketclient.authenticated=true;
    socketclient.emit("authenticated")
    var welcomemessage = username + " has joined the chat system!";
    console.log(welcomemessage);
    SendToAuthenticatedClient(socketclient, "welcome", welcomemessage);
}
    });

    socketclient.on("chat", (message)=>
{
    if(!socketclient.authenticated)
    {
        console.log("Unauthenticated client sent a chat, Supress!");
    }
    var chatmessage = socketclient.username + " says:  " + message;
    console.log(chatmessage);
    // socketio.sockets.emit("chat", chatmessage)
     SendToAuthenticatedClient(undefined, "chat",chatmessage);

});

var DataLayer = {
    info: 'Data Layer Implementation for Messenger',
        checklogin(username, password){
            //hardcoded our names and passwords
            if(password == "12345" && username == "Bradley" || password == "12345" && username == "Ali")  
            {
                console.log("checklogin: " + username  + " / " + password);
                console.log("Just for testing - return true ");                
                return true;
            }

            else
            {
                var errormessage = "The username or password is incorrect."
                console.log(errormessage);
                socketclient.emit("Invalid login",errormessage);
                return false;
            }
          
          
            
        
        }
    }

    function SendToAuthenticatedClient(sendersocket,type,data){
        var sockets = socketio.sockets.sockets;
        for(var socketId in sockets){
            var socketclient = sockets[socketId];
            if(socketclient.authenticated){
                socketclient.emit(type,data);
                var logmsg= "Debug:>sent to " + socketclient.username + " with ID= " + socketId;
                console.log(logmsg);
            }
        }
    }



});
