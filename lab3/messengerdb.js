const MongoClient = require('mongodb').MongoClient;
const uri = "mongodb+srv://Ruckerb3:Bluegreen5@messengerdb.d18ij.mongodb.net/Messenger?retryWrites=true&w=majority";
const mongodbclient = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
let db = null;
mongodbclient.connect( (err,connection) => {
    if(err) throw err;
    console.log("Connected to the MongoDB cluster!");
    db = connection.db();
})
const dbIsReady = ()=>{
    return db != null;
};
const getDb = () =>{
    if(!dbIsReady())
        throw Error("No database connection");
    return db;
}
const  checklogin = async (username)=>{
   var users = getDb().collection("users");
   var user = await users.findOne({username:username});
   //console.log(JSON.stringify(user.username));
   if(user != null && user.username==username)
   {
       console.log("Debug>messengerdb.checklogin-> user found \n" + JSON.stringify(user));
       return true;
   }
    console.log("Debug>messengerdb.checklogin-> user"  +   " not found \n");
    return false;
}
const addUser = async (username,password)=>{
    var users = getDb().collection("users");
    var user = await users.findOne({username:username});
    if(user!=null && user.username==username)
    {
    console.log(`Debug>messenger.addUser: Username  ' ${username}' exists!`);
    return "UserExist";
    }
    const newUser = {"username": username,"password" : password}
    try{
        const result = await users.insertOne(newUser);
        if(result!= null){
            console.log("Debug>messengerdb.addUser: a new user added: \n", result);
            return "Success";
        }

    }catch{
        console.log("Debug>messengerdb.addUser: error for adding " +
        username + "':\n",err);
        return "Error";
    }



}
module.exports = {checklogin,addUser};